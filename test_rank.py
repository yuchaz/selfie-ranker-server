from keras import backend as K
K.set_image_dim_ordering('th')
from keras.optimizers import SGD
from keras.models import load_model

import cv2, numpy as np
model = load_model("weights/selfi-01-0.28.f5")
sgd = SGD(lr=1e-4, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(optimizer=sgd, loss='mean_squared_error')

def read_test_image_under_folder(images):
    num = len(images)
    inputs = np.zeros((num,3,224,224))
    for i in range(num):
        im = cv2.resize(images[i], (224, 224)).astype(np.float32)
        im[:,:,0] -= 103.939
        im[:,:,1] -= 116.779
        im[:,:,2] -= 123.68
        im = im.transpose((2,0,1))
        inputs[i] = im
    return inputs, num

def inference(images):
    inputs, num = read_test_image_under_folder(images=images)
    if num <1:
        assert "no image to infer"
    score = model.predict(inputs)

    return score


if __name__ == "__main__":
    inference()
