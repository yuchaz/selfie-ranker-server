import argparse
import cv2
import numpy as np
import glob

threshold = 35

def variance_of_laplacian(image):
	return cv2.Laplacian(image, cv2.CV_64F).var()

def test_if_blur(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    fm = variance_of_laplacian(gray)
    if fm < threshold:
        return True
    else:
        return False

def get_blur_score(images):
	return [ variance_of_laplacian(image) for image in images ]

def get_non_blur_id (images):
    return np.array([ not_blur_id for not_blur_id,image in enumerate(images) if not test_if_blur(image) ])

def read_img_under_a_folder(filename_pattern):
	image_names = glob.glob(filename_pattern)
	gray_images = [ cv2.imread(img_name, cv2.COLOR_BGR2GRAY) for img_name in image_names ]
	for idx, gray_img in enumerate(gray_images):
		score = variance_of_laplacian(gray_img)
		print ("Image filename: {}, score: {}".format(
			image_names[idx].split('/')[-1], score))

if __name__ == '__main__':
	read_img_under_a_folder('/Users/YuChia/datasets/blur_imgs/*.jpg')
