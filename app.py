from flask import Flask, jsonify, request
import numpy as np
import cv2
import os
from blur_detection import get_non_blur_id, get_blur_score
from test_rank import inference
import glob
import time

app = Flask(__name__)

key_template = "image_{}"
video_seconds = 5
total_frames = 30
keys = [ key_template.format(idx) for idx in np.arange(total_frames) ]

output_dir = "./output_{}"

def check_file_type(filename):
    return filename.endswith(('.jpg','.png'))

def convert_flaskfile_to_img(img):
    return cv2.imdecode(np.fromstring(img.read(), np.uint8), cv2.CV_LOAD_IMAGE_COLOR)

def process_imgs(images):
    return [ convert_flaskfile_to_img(img) for img in images ]

def save_images(images):
    files = glob.glob("./test_img/*.jpg")
    for f in files:
        os.remove(f)
    for idx,img in enumerate(images):
        cv2.imwrite("./test_img/image_{}.jpg".format(idx), img)

@app.route('/rank', methods=['POST'])
def rank():
    print ("Request received.")
    start = time.time()
    images = [ request.files[key] for key in keys ]
    cv_images = process_imgs(images)

    non_blur_id = get_non_blur_id(cv_images)

    scores = inference(cv_images)
    scores_all = np.zeros(total_frames)
    for idx in range(non_blur_id.shape[0]):
        scores_all[non_blur_id[idx]] = scores[idx]
    print time.time()-start
    return jsonify(scores_all.tolist())

@app.route('/all', methods=['POST'])
def blur_scores():
    print ("Request received.")
    start = time.time()
    images = [ request.files[key] for key in keys ]
    cv_images = process_imgs(images)

    non_blur_id = get_non_blur_id(cv_images)
    blur_scores = get_blur_score(cv_images)

    scores = inference(cv_images)
    scores_all = np.zeros(total_frames)
    for idx in range(non_blur_id.shape[0]):
        scores_all[non_blur_id[idx]] = scores[idx]
    print time.time()-start
    return jsonify([ [ scores_all[idx], blur_scores[idx] ] for idx in range(scores_all.shape[0]) ])

if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        port=int("80"),
        debug=True
    )
